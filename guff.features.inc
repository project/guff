<?php

/**
 * Implements hook_features_export
 * 
 * Set the depenencies of features using this to require this module
 *
 * Just copy the data from the input to the output.
 */
function guff_features_export($data, &$export, $module_name) {

  $export['dependencies']['guff'] = 'guff';

  // The following is the simplest implementation of a straight object export
  // with no further export processors called.
  foreach ($data as $component) {
    $export['features']['guff'][$component] = $component;
  }
  return array();
}

/**
 * Implements hook_features_export_options
 *
 * Returns an options array for all uuid files keyed by uuid with the title being
 * The usage count of the file and the uri of the file
 */
function guff_features_export_options() {
  $query = db_select('file_managed','f');
  $query->leftJoin('file_usage','u','f.fid=u.fid && u.module!=\'guff\'');
  $query->condition('f.uuid','','<>');
  $query->condition('f.status',1);
  $query->fields('f',array('uuid','uri'));
  $query->addExpression('sum(u.count)','count');
  $query->orderBy('f.uri');
  $query->groupBy('f.fid');
  
  $result = array();
  foreach ($query->execute() as $row) { 
    $count = 
    $result[$row->uuid] = '('.(empty($row->count)?0:$row->count).') '.$row->uri;
  }
  return $result;
}

/**
 * Implements hook_features_export_render
 *
 * Load all the file from the uuids supplied in $data
 *
 * For each file generate the an output structure and append that the the code 
 * array. Additionally if exporting, base64 encode the file data and add it to
 * its own output file $module_name.guff.$uuid.inc
 *
 */
function guff_features_export_render($module, $data, $export) {
  $files = array();
  $code = array();
  
  foreach (entity_uuid_load('file',$data) as $file) {

    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    
    $code_file = new stdClass();
    $code_file->uuid = $file->uuid;
    $code_file->filedir = $wrapper->dirname($file->uri);
    $code_file->filename = $file->filename;
    $code_file->filesize = $file->filesize;
    $code_file->filemime = $file->filemime;
    $code_file->type = $file->type;
    $code_file->guff_timestamp = (string)$file->timestamp;

    if (substr_compare($code_file->filedir,'/',-1,1)) $code_file->filedir .= '/';
    
    if ($wrapper instanceof DrupalLocalStreamWrapper) {
      $datafile = 'guff.'.$file->uuid;
      $code_file->datatype = 'include';
      $code_file->data = $datafile;
      
      if ($export) {
        $contents = file_get_contents($wrapper->realpath());
        if ($contents === FALSE) 
          continue; // Filedata missing
        $code_file->filesize = strlen($contents);
        $code_file->md5 = md5($contents);
        $files['guff_'.str_replace('-','_',$file->uuid)] = array(
          'file' => $datafile,
          'code' => 'return base64_decode('.var_export("\r\n".chunk_split(base64_encode($contents)),TRUE).');',
          // Line above encodes file data as base64. The line below will output it as a binary string
          //'code' => 'return '.var_export($contents,TRUE).';',
        );
        unset($contents);
      }
      else {
        $code_file->md5 = @md5_file($wrapper->realpath());
        if (empty($code_file->md5)) $code_file->md5 = "*** FILE MISSING ***";
      }
    }
    else {
      // TODO - Add support for other file stream types
      continue;
    }
        
    $code_file->filesize = (int) $code_file->filesize;
    $code[$file->uuid] = $code_file;    
  }
  
  $code = "  return ". features_var_export($code, '  ') .";";  
  $files['guff_default_files'] = $code;
  
  return $files;
}


/**
 * Implements hook_features_revert
 *
 * Just calls guff_features_rebuild()
 */
function guff_features_revert($module_name) {
  guff_features_rebuild($module_name);
}

/**
 * Implements hook_features_rebuild
 *
 * Calls the hook_guff_default_files for the given module
 * 
 * For each file, if a managed file already exsits, replace it, otherwise 
 * create a new managed file and output the filedata to the uri of the new or 
 * existing managed file. Increment the file usage if needed and clear any 
 * image style variations
 *
 * Any files used to be owned by the feature module but aren't any longer have 
 * their usage count decremented.
 */
function guff_features_rebuild($module_name) {
  global $user;
  
  $usage = db_select('file_usage', 'f')
      ->fields('f', array('fid'))
      ->condition('module', 'guff')
      ->condition('type', $module_name)
      ->condition('count', 0, '>')
      ->execute()->fetchAllKeyed(0,0);
    
  $guff_default_files = module_invoke($module_name, 'guff_default_files');
  if (!empty($guff_default_files)) {
    foreach ($guff_default_files as $file) {
    
      $file = (object) $file;
      $existing = entity_uuid_load('file',array($file->uuid));
      
      if (!empty($existing)) $existing = reset($existing);
      else $existing = null;

      $destination = $file->filedir . $file->filename;
      
      $replace = (empty($existing->uri)||$existing->uri!=$destination)?FILE_EXISTS_RENAME:FILE_EXISTS_REPLACE;
      
      if ($file->datatype == 'include') {      
        module_load_include('inc',$module_name,$module_name.'.'.$file->data);
        $contents = module_invoke($module_name,'guff_'.str_replace('-','_',$file->uuid));
        $destination = file_unmanaged_save_data($contents,$destination,$replace);
        $file->filesize = strlen($contents);
      }
      else {
        // TODO - Add support for other file stream types
        continue;
      }      
      
      $file->uri = $destination;
      $file->status = 1;
      $file->uid = $user->uid;
      
      if ($existing) $file->fid = $existing->fid;
      
      entity_uuid_save('file',$file);
      
      if (empty($usage[$file->fid])) 
        file_usage_add($file,'guff',$module_name,0);
      else 
        unset($usage[$file->fid]);      
      
      image_path_flush($file->uri);
    }
  }
  if ($usage) {
    foreach (file_load_multiple($usage) as $file) {
      file_usage_delete($file,'guff',$module_name);
    }
  }
}

/** 
 * Implements hook_features_disable_feature 
 *
 * Clears file usage for all files owned by the feature module being disabled
 */
function guff_features_disable_feature($module_name) {
  // Delete file usage counts for all files that the module imported
  $usage = db_select('file_usage', 'f')
      ->fields('f', array('fid'))
      ->condition('module', 'guff')
      ->condition('type', $module_name)
      ->condition('count', 0, '>')
      ->execute()->fetchAllKeyed(0,0);
    
  if ($usage) {
    foreach (file_load_multiple($usage) as $file) {
      file_usage_delete($file,'guff',$module_name);
    }
  }
}

/**
 * Implements hook_features_pipe_alter
 *
 * Acts on field, uuid_term and uuid_node components
 *
 * When acting on field, add the default image uuids
 *
 * When acting on uuid_term and uuid_node, all file and image fields uuids will be added
 */
function guff_features_pipe_alter(&$pipe, &$data, &$export) {
  $uuids = array();

  // Field so export image field default_image uuids
  if ($export['component'] == 'field') {
    foreach ($data as $identifier) {
      if ($field = features_field_load($identifier)) {
        if ($field['field_config']['type'] == 'image') {
          $default_config = $field['field_config']['settings']['default_image'];
          $default_instance = $field['field_instance']['settings']['default_image'];
          
          if ($default_config || $default_instance) {
            foreach (entity_get_uuid_by_id('file',array($default_config,$default_instance)) as $uuid) {
              $uuids[$uuid] = $uuid;
            }
          }
        }
      }
    }
  }
  // Otherwise handle uuid entities (node and term for now) and add file and image field uuids
  else {
    if ($export['component'] == 'uuid_node') $entity_type = 'node';
    else if ($export['component'] == 'uuid_term') $entity_type = 'taxonomy_term';
    else return;   
    
    $entities = entity_uuid_load($entity_type,$data);
    foreach($entities as $entity) {
      list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
      
      foreach (field_info_instances($entity_type, $bundle) as $field_name => $instance) {
        $field = field_info_field($field_name);
        if ($field['type'] == 'file' || $field['type'] == 'image') {
          if (isset($entity->$field_name)) {
            foreach ($entity->$field_name as $lang => $values) {
              foreach ($values as $value) {              
                if (!empty($value['uuid'])) $uuids[$value['uuid']] = $value['uuid'];
              }
            }
          }        
        }
      }
    }
  }
  // Any uuids? if so add them to the pipe
  if ($uuids) 
    $pipe = array_merge_recursive($pipe, array('guff' => $uuids));
}
