GUFF (generic uuid file features) is a module that provides support for
exporting file and image data into features.

Features, UUID and Entity API are requried.

When enabled, the module will automatically detect when a file field or image
field is being exported and add the file data to the feature. The module will
also export the data for any default images set on image fields. The module 
also lets you manually export individual file data into features.

Due to limitations with Features, file and image data is stored in separate php
include files encoded using base64. Ideally the files would be stored as direct
copies, but it isn't possible to do automatically without changes to Features.
